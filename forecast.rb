require 'pry'
require 'forecast_io'

ForecastIO.configure do |c|
  c.api_key = 'c3c46f2ceb471e5d5bf6cd29b5708bfe'
end

# binding.pry
forecast = ForecastIO.forecast(48.85837009999999, 2.2944813)

f = forecast.currently

puts f.summary
puts ((f.apparentTemperature - 32) * 5 / 9).round(1)
