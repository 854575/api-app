require 'pry'
require 'stock_quote'


input = ARGV

input.each do |c|
  stock = StockQuote::Stock.quote(c)
  puts "#{stock.name} is $#{stock.last_trade_price_only}"
end