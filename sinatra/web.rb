require 'sinatra'
require 'stock_quote'

get '/' do
  @content = 'Hack Your Life'
  erb :index
end

post '/result' do
  @content = stock_price(params[:company])
  erb :index
end

def stock_price(company)
  stock = StockQuote::Stock.quote company

  "#{stock.name} is $#{stock.last_trade_price_only}"
end

# C9 에서 돌리면,
# $ ruby web.rb -p $PORT -o $IP